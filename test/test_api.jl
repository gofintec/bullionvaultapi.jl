@testset "api.jl" begin

Mocking.activate()

@test 1 == 1


# Return error message
p_404 = @patch BullionVaultAPI.request(
    method::AbstractString,
    url::AbstractString,
) = HTTP.Response(404)
apply(p_404) do
    @test BullionVaultAPI.login("USR", "PWD") == false
    @test BullionVaultAPI.view_market(true) == DataFrame()
    @test BullionVaultAPI.cancel_order("", false) == DataFrame()
    @test BullionVaultAPI.place_order("", "", "", "", "", "", "", true) == DataFrame()
    @test BullionVaultAPI.view_balance() == [DataFrame(), DataFrame(), DataFrame()]
    @test BullionVaultAPI.view_orders() == DataFrame()
    @test BullionVaultAPI.view_single_order("", "") == DataFrame()
    @test BullionVaultAPI.view_weight_unit() == ""
    @test BullionVaultAPI.update_weight_unit("") == ""
end


# Test login function
p_200 = @patch BullionVaultAPI.request(
    method::AbstractString,
    url::AbstractString,
) = HTTP.Response(200, ["X-Challenge" => "0,2,5"])
apply(p_200) do
    @test BullionVaultAPI.login("USR", "PWD") == true
end


# Test view market function
p_view_market = @patch BullionVaultAPI.api_request(
    method::AbstractString,
    url::AbstractString,
) = Dict(
    :status => 200,
    :body => read(joinpath(@__DIR__, "response/view_market.xml"), String),
)
apply(p_view_market) do
    @test BullionVaultAPI.view_market(false, "USD", "AUXLN") == DataFrame(
        actionIndicator=["B", "B", "B", "S",  "S", "S"],
        considerationCurrency="USD",
        limit=["12510", "12500", "12490", "12590", "12600", "12610"],
        quantity=["0.1", "0.2", "0.1", "0.2", "0.1", "0.1"],
        securityId="AUXLN",
    )
end


# Test cancel order function (while logged in)
p_cancel_order = @patch BullionVaultAPI.api_request(
    method::AbstractString,
    url::AbstractString,
) = Dict(
    :status => 200,
    :body => read(joinpath(@__DIR__, "response/cancel_order.xml"), String),
)
apply(p_cancel_order) do
    @test BullionVaultAPI.cancel_order("19223", true) == DataFrame(
        actionIndicator=["B"],
        clientTransRef=["CLIENT119222"],
        considerationCurrency=["USD"],
        goodUntil=[""],
        lastModified=["2017-09-18 08:06:15 UTC"],
        limit=["12679"],
        orderId=["19223"],
        orderTime=["2017-09-18 08:06:15 UTC"],
        orderValue=["1267.9"],
        quantity=["0.1"],
        quantityMatched=["0"],
        securityId=["AUXZU"],
        statusCode=["CANCELLED"],
        totalCommission=["0"],
        totalConsideration=["0"],
        tradeType=["ORDER_BOARD_TRADE"],
        typeCode=["TIL_CANCEL"],
    )
end


# Test cancel order function (while logged in)
p_cancel_order = @patch BullionVaultAPI.api_request(
    method::AbstractString,
    url::AbstractString,
) = Dict(
    :status => 200,
    :body => "Some Random, not XML String. You should have logged in.",
)
apply(p_cancel_order) do
    @test BullionVaultAPI.cancel_order("19223", true) == DataFrame()
end


# Test place order function (while logged in)
p_place_order = @patch BullionVaultAPI.api_request(
    method::AbstractString,
    url::AbstractString,
) = Dict(
    :status => 200,
    :body => read(joinpath(@__DIR__, "response/place_order.xml"), String),
)
apply(p_place_order) do
    @test BullionVaultAPI.place_order(
        "B",
        "EUR",
        "AGXZU",
        "0.001",
        "100",
        "TIL_TIME",
        "XRAY15782055",
        true,
        "2005-06-02%2009:15%20UTC",
    ) == DataFrame(
        actionIndicator=["B"],
        clientTransRef=["asdf"],
        considerationCurrency=["USD"],
        goodUntil=[""],
        lastModified=["2005-06-02 14:14:25 UTC"],
        limit=["13500"],
        orderId=["1080"],
        orderTime=["2005-06-02 14:14:24 UTC"],
        quantity=["0.001"],
        quantityMatched=["0.001"],
        securityId=["AUXLN"],
        statusCode=["DONE"],
        totalCommission=["0.11"],
        totalConsideration=["12.59"],
        tradeType=["ORDER_BOARD_TRADE"],
        typeCode=["TIL_CANCEL"],
    )
end


# Test place order function (while logged in)
p_place_order = @patch BullionVaultAPI.api_request(
    method::AbstractString,
    url::AbstractString,
) = Dict(
    :status => 200,
    :body => "Some Random, not XML String. You should have logged in.",
)
apply(p_place_order) do
    @test BullionVaultAPI.place_order("", "", "", "", "", "", "", true) == DataFrame()
end


# Test view market function
p_view_balance = @patch BullionVaultAPI.api_request(
    method::AbstractString,
    url::AbstractString,
) = Dict(
    :status => 200,
    :body => read(joinpath(@__DIR__, "response/view_balance.xml"), String),
)
df_client_position = DataFrame(
    available=["3.026", "5"],
    classNarrative=["GOLD", "GOLD"],
    securityId=["AUXLN", "AUXNY"],
    total=["3.026", "5"],
    totalValuation=["40578.66", "67050"],
    valuationCurrency=["USD", "USD"],
)
df_pending_settlements = DataFrame(
    classNarrative=["GOLD", "GOLD"],
    pendingTransferId=[1, 2],
    securityId=["AUXZU", "AUXLN"],
    total=["250.919", "0.92"],
    totalValuation=["3181652.92", "11656.4"],
    valuationCurrency=["USD", "USD"],
)
df_pending_transfers = DataFrame(
    balance=["200", "50", "0.92"],
    dueDate=[
        "2012-10-18 00:00:00 UTC",
        "2012-10-18 00:00:00 UTC",
        "2012-11-08 00:00:00 UTC",
    ],
    lowestLedger=["UNS_RCT", "UNS_RCT", "UNS_RCT"],
    pendingTransferId=[1, 1, 2],
    type=["OFF_MARKET_TRADE", "OFF_MARKET_TRADE", "OFF_MARKET_TRADE"],
    valuation=["2536000", "634000", "11656.4"],
    valuationCurrency=["USD", "USD", "USD"],
)
apply(p_view_balance) do
    @test BullionVaultAPI.view_balance(false) == [
        df_client_position,
        df_pending_settlements,
        df_pending_transfers,
    ]
end



# Test view orders function
p_view_orders = @patch BullionVaultAPI.api_request(
    method::AbstractString,
    url::AbstractString,
) = Dict(
    :status => 200,
    :body => read(joinpath(@__DIR__, "response/view_orders.xml"), String),
)
apply(p_view_orders) do
    @test BullionVaultAPI.view_orders(
        "AUXLN",
        "USD",
        "OPEN",
        "20130921",
        "20130925",
        0,
    ) == DataFrame(
        actionIndicator=["B", "B", "B", "B"],
        clientId=["******", "******", "******", "******"],
        clientTransRef=["asdf", "050520115557474", "050520120214131", "abc123"],
        considerationCurrency=["USD", "USD", "USD", "GBP"],
        goodUntil=["", "", "", ""],
        lastModified=[
            "2005-06-02 14:14:25 UTC",
            "2005-05-20 15:59:45 UTC",
            "2005-05-20 12:02:17 UTC",
            "2005-05-19 09:21:21 UTC",
        ],
        limit=["13500", "13400", "13500", "7300"],
        orderId=["1080", "1061", "1041", "1000"],
        orderTime=[
            "2005-06-02 14:14:24 UTC",
            "2005-05-20 15:59:33 UTC",
            "2005-05-20 12:02:16 UTC",
            "2005-05-19 09:21:21 UTC",
        ],
        quantity=["0.001", "0.002", "0.002", "0.1"],
        quantityMatched=["0.001", "0.002", "0.002", "0.025"],
        securityId=["AUXLN", "AUXNY", "AUXNY", "AUXLN"],
        statusCode=["DONE", "DONE", "DONE", "CANCELLED"],
        totalCommission=["0.11", "0", "0", "1.46"],
        totalConsideration=["12.59", "26.8", "27", "182.5"],
        tradeType=[
            "ORDER_BOARD_TRADE",
            "ORDER_BOARD_TRADE",
            "ORDER_BOARD_TRADE",
            "ORDER_BOARD_TRADE",
        ],
        typeCode=["TIL_CANCEL", "TIL_CANCEL", "TIL_CANCEL", "TIL_CANCEL"],
    )
end


# Test view single order function (while logged in)
p_view_single_order = @patch BullionVaultAPI.api_request(
    method::AbstractString,
    url::AbstractString,
) = Dict(
    :status => 200,
    :body => read(joinpath(@__DIR__, "response/view_single_order.xml"), String),
)
apply(p_view_single_order) do
    @test BullionVaultAPI.view_single_order("1207516", "BOTAUX20160905131249") == DataFrame(
        actionIndicator=["B"],
        clientTransRef=["asdf"],
        considerationCurrency=["USD"],
        goodUntil=[""],
        lastModified=["2005-06-02 14:14:25 UTC"],
        limit=["13500"],
        orderId=["1080"],
        orderTime=["2005-06-02 14:14:24 UTC"],
        quantity=["0.001"],
        quantityMatched=["0.001"],
        securityId=["AUXLN"],
        statusCode=["DONE"],
        totalCommission=["0.11"],
        totalConsideration=["12.59"],
        tradeType=["ORDER_BOARD_TRADE"],
        typeCode=["TIL_CANCEL"],
    )
end


# Test view single order function (while logged in)
p_view_single_order = @patch BullionVaultAPI.api_request(
    method::AbstractString,
    url::AbstractString,
) = Dict(
    :status => 200,
    :body => "Some Random, not XML String. You should have logged in.",
)
apply(p_view_single_order) do
    @test BullionVaultAPI.view_single_order("") == DataFrame()
end


# Test view weight unit setting function (while logged in)
p_view_weight_unit = @patch BullionVaultAPI.api_request(
    method::AbstractString,
    url::AbstractString,
) = Dict(
    :status => 200,
    :body => read(joinpath(@__DIR__, "response/view_weight_unit.xml"), String),
)
apply(p_view_weight_unit) do
    @test BullionVaultAPI.view_weight_unit() == "KG"
end


# Test update weight unit setting function (while logged in)
p_update_weight_unit = @patch BullionVaultAPI.api_request(
    method::AbstractString,
    url::AbstractString,
) = Dict(
    :status => 200,
    :body => read(joinpath(@__DIR__, "response/update_weight_unit.xml"), String),
)
apply(p_update_weight_unit) do
    @test BullionVaultAPI.update_weight_unit("KG") == "KG"
end


# Test downtime function
p_downtime = @patch BullionVaultAPI.api_request(
    method::AbstractString,
    url::AbstractString,
) = Dict(
    :status => 200,
    :body => read(joinpath(@__DIR__, "response/downtime.xml"), String),
)
apply(p_downtime) do
    @test BullionVaultAPI.downtime() == Dict(
        "title"       => "BullionVault Planned Downtie",
        "link"        => "https://www.bullionvault.com",
        "description" => "Upcoming BullionVault Planned Downtime",
    )
end

end
