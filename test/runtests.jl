using BullionVaultAPI
using Mocking
using Test
using DataFrames
using HTTP

@testset "BullionVaultAPI" begin
    include("test_request.jl")
    include("test_api.jl")
end
