@testset "request.jl" begin

Mocking.activate()

@test 1 == 1


# Test post with success response and actual dependency
response = BullionVaultAPI.request("POST", "https://duckduckgo.com/")
@test response.status == 200

# Test with success response
p_200 = @patch BullionVaultAPI.request(
    method::AbstractString,
    url::AbstractString,
) = HTTP.Response(200, ["X-Challenge" => "0,2,5"])
apply(p_200) do
    @test BullionVaultAPI.api_request("POST", "https://duckduckgo.com/") == Dict(
        :status => 200,
        :challenge => "0,2,5",
        :body => "",
    )
end

# Test with failure
p_404 = @patch BullionVaultAPI.request(
    method::AbstractString,
    url::AbstractString,
) = HTTP.Response(404)
apply(p_404) do
    @test BullionVaultAPI.api_request("POST", "https://duckduckgo.com/") == Dict(
        :status => 404,
        :challenge => "",
        :body => "",
    )
end

# Test with throw(exception)
p_throw = @patch BullionVaultAPI.request(
    method::AbstractString,
    url::AbstractString,
) = nothing
apply(p_throw) do
    @test BullionVaultAPI.api_request("POST", "https://duckduckgo.com/") == Dict(
        :status => 404,
        :challenge => "",
        :body => "",
    )
end

end
