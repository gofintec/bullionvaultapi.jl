[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://gofintec.gitlab.io/bullionvaultapi.jl/dev)
[![pipeline status](https://gitlab.com/gofintec/bullionvaultapi.jl/badges/master/pipeline.svg)](https://gitlab.com/gofintec/bullionvaultapi.jl/-/commits/master)
[![coverage report](https://gitlab.com/gofintec/bullionvaultapi.jl/badges/master/coverage.svg)](https://gitlab.com/gofintec/bullionvaultapi.jl/-/commits/master)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Code Style: Blue](https://img.shields.io/badge/code%20style-blue-4495d1.svg)](https://github.com/invenia/BlueStyle)
[![ColPrac: Contributor Guide on Collaborative Practices for Community Packages](https://img.shields.io/badge/ColPrac-Contributor%20Guide-blueviolet)](https://github.com/SciML/ColPrac)



# BullionVaultAPI

**This package is an API implementation of [BullionVault](https://www.bullionvault.com/help/xml_api.html) using [Julia](https://julialang.org/)**


## Welcome

Feel free to use this Software. 
Nevertheless, it deals with your real account and real money. Be careful and double check what you are doing. It is still under early stages development.

If you miss any feature, find a bug or just have a question. Feel free to file an issue or send me a message, I will come back to you.

## Installation

Install with the Julia package manager [Pkg](https://pkgdocs.julialang.org/), just like any other registered Julia package:

```jl
pkg> add BullionVaultAPI  # Press ']' to enter the Pkg REPL mode.
```
or

```jl
julia> using Pkg; Pkg.add("BullionVaultAPI")
```

## API Feature List

 - Login
    - Create a new session
    - Memorable information check (if enabled by user)
 - View the market
    - If logged in
    - If not logged in
 - Place an order
 - Cancel an order
 - View balance
 - View orders
 - View single order
 - View weight unit setting
 - Update weight unit setting
 - API unavailable check (RSS Feed)


## Future Idea Collection

 - Limit access per time
 - Catch input-type errors prior sending
 - Delete unnecessary tables/informations
 - Provide cookies for multiple parallel logins
 - Catch problems while not logged in but accidentially send a request (LightXML feature/bug?)
 - Change API function types from AbstractString to actual type
 - Add specific error messages
 - Add functions which throw errors and alternative without (as currently is)


 ## Known Issues



## Contributing

Contributions are very welcome, as are feature requests and suggestions. Please open an [issue](https://gitlab.com/gofintec/bullionvaultapi.jl/-/issues) if you encounter any problems.

## License

This project is licensed under the [MIT LICENSE](LICENSE.md).

## Acknowledgments

- [BullionVault](https://www.bullionvault.com/help/xml_api.html) for providing the API

[![Buy Me a Coffe](https://www.buymeacoffee.com/assets/img/custom_images/yellow_img.png)](https://www.buymeacoffee.com/gogriebel)