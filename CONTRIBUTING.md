# Contributing

If you miss any feature, find a bug or just have a question. Feel free to file an issue or send me a message, I will come back to you.

This package uses unit testing to cover 100% of the contributions.

Please follow the style guidelines of [![Code Style: Blue](https://img.shields.io/badge/code%20style-blue-4495d1.svg)](https://github.com/invenia/BlueStyle)
