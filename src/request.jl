"""
    request(method::AbstractString, url::AbstractString) -> HTTP.Response()

Abstraction to actual HTTP request function

# Arguments
- `method::AbstractString`: to connect to the server, either `"GET"` or `"POST"`
- `url::AbstractString`: the requested `url` String

# Returns
- `HTTP.Response()`: The response of the used HTTP package
"""
function request(
    method::AbstractString,
    url::AbstractString,
)
    return HTTP.request(method, url; cookies=true)
end


"""
    api_request(
        method::AbstractString,
        url::AbstractString,
    ) -> Dict(:status => Int, :challenge => String, :body => String)

Request to API to the given `url`. Returned values are filtered by `:status` and `:body`.
In case of failure, `:status` code `404` is returned with an empty body.

# Arguments
- `method::AbstractString`: to connect to the server, either `"GET"` or `"POST"`
- `url::AbstractString`: the requested url String

# Returns
- `Dict(:status => Int, :challenge => String, :body => String)`: request response filtered
"""
function api_request(method::AbstractString, url::AbstractString)
    @show url
    try
        # request url
        # Mockable for unit-testing
        response = @mock request(method, url)

        challenge = ""
        challenges = filter(x -> x.first == "X-Challenge", response.headers)
        for c in challenges
            challenge = c.second
        end

        return Dict(
            :status => Int(response.status),
            :challenge => String(challenge),
            :body => String(response.body),
        )
    catch e
        return Dict(
            :status => Int(404),
            :challenge => String(""),
            :body => String(""),
        )
    end
end
