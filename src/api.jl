"""
    login(usr::AbstractString, pwd::AbstractString) -> Bool

login function to start new session

# Arguments
- `usr::AbstractString`: BullionVault user name
- `pwd::AbstractString`: BullionVault password

# Returns
- `Bool`: success indicator
"""
function login(usr::AbstractString, pwd::AbstractString)
    # Create url to be requested
    url = "https://www.bullionvault.com/secure/"
    url *= "j_security_check?"
    url *= "j_username=" * usr
    url *= "&j_password=" * pwd

    # api access
    # Mockable for unit-testing
    response = @mock api_request("POST", url)

    # Second login challenge
    if (!isempty(response[:challenge]))
        # Create second login url to be requested
        url = "https://www.bullionvault.com/secure/second_login.do?"
        das = response[:challenge]
        val = rsplit(das, ",")
        dict = map(
            i -> "response[$(i-1)]=" * Char('A' + parse(Int, val[i])),
            eachindex(val),
        )
        url *= join(dict, "&")

        # api access
        # Mockable for unit-testing
        response = @mock api_request("POST", url)
    end

    return (response[:status] == 200) ? true : false
end


"""
    view_market(
        loggedin::Bool,
        considerationCurrency::AbstractString="",
        securityId::AbstractString="",
        quantity::AbstractString="0.001",
        marketWidth::Integer=1
    ) -> DataFrame

View the market

# Arguments
- `loggedin::Bool`: The non-logged in version is cached on the server and is not as
    up-to-date as the logged in version.
- `considerationCurrency::AbstractString=""`: The currency to view. May be one of `USD`,
    `GBP`, `EUR` or blank for all.
- `securityId::AbstractString=""`: The ID of the security to view. May be any valid
    `security ID`, or blank for all.
- `quantity::AbstractString="0.001"`: The minimum quantity to show. A value of 0.005 will
    filter out all bids/offers smaller than 5 grams. Use 0.001 to show all bids/offers.
- `marketWidth::Integer=1`: The maximum number of bids and offers returned for each market.
    The default value is 1.

# Returns
- `DataFrame`: containing market details as shown in the structure below:
```JuliaIO
DataFrame(
    actionIndicator=["B", "B", "B", "S",  "S", "S"],
    considerationCurrency="USD",
    limit=["12510", "12500", "12490", "12590", "12600", "12610"],
    quantity=["0.1", "0.2", "0.1", "0.2", "0.1", "0.1"],
    securityId="AUXLN"
)
```
"""
function view_market(
    loggedin::Bool,
    considerationCurrency::AbstractString="",
    securityId::AbstractString="",
    quantity::AbstractString="0.001",
    marketWidth::Integer=1,
)
    # Create request url string
    url = "https://www.bullionvault.com/"
    url *= loggedin ? "secure/api/v2/" : ""
    url *= "view_market_xml.do"
    first = true
    if (!isempty(considerationCurrency))
        first = false
        url *= "?considerationCurrency=" * considerationCurrency
    end
    if (!isempty(securityId))
        url *= first ? "?" : "&"
        first = false
        url *= "securityId=" * securityId
    end
    url *= first ? "?" : "&"
    url *= "quantity=" * string(quantity)
    url *= "&marketWidth=" * string(marketWidth)

    # api access
    # Mockable for unit-testing
    response = @mock api_request("POST", url)

    # Dataframes for results
    df_prices = DataFrame()

    # Parse return on success
    if (response[:status] == 200)
        # Check https://github.com/JuliaIO/LightXML.jl for reference

        # Step through tree to client balance
        xdoc = parse_string(response[:body])
        envelope = root(xdoc)
        message = find_element(envelope, "message")
        market = find_element(message, "market")

        # Add all client positions to the data frame
        pitches = find_element(market, "pitches")
        for pitch in child_elements(pitches)
            dict_pitch = attributes_dict(pitch)

            # Add all buy prices to the data frame
            buyPrices = find_element(pitch, "buyPrices")
            for price in child_elements(buyPrices)
                dict_price = attributes_dict(price)
                content = merge(dict_pitch, dict_price)
                df = DataFrame(content)
                append!(df_prices, df)
            end

            # Add all sell prices to the data frame
            sellPrices = find_element(pitch, "sellPrices")
            for price in child_elements(sellPrices)
                dict_price = attributes_dict(price)
                content = merge(dict_pitch, dict_price)
                df = DataFrame(content)
                append!(df_prices, df)
            end
        end
    end

    return df_prices
end


"""
    cancel_order(orderId::AbstractString, confirmed::Bool) -> DataFrame

Cancel order

# Arguments
- `orderId::AbstractString`: The order ID returned by `place_order`.
- `confirmed::Bool`: For bots, this value must always be `true`.

# Returns
- `DataFrame`: containing order details if successful, otherwise it is empty. Content as
    shown in the structure below:
```JuliaIO
DataFrame(
    actionIndicator=["B"],
    clientTransRef=["CLIENT119222"],
    considerationCurrency=["USD"],
    goodUntil=[""],
    lastModified=["2017-09-18 08:06:15 UTC"],
    limit=["12679"],
    orderId=["19223"],
    orderTime=["2017-09-18 08:06:15 UTC"],
    orderValue=["1267.9"],
    quantity=["0.1"],
    quantityMatched=["0"],
    securityId=["AUXZU"],
    statusCode=["CANCELLED"],
    totalCommission=["0"],
    totalConsideration=["0"],
    tradeType=["ORDER_BOARD_TRADE"],
    typeCode=["TIL_CANCEL"],
)
```
"""
function cancel_order(orderId::AbstractString, confirmed::Bool)
    # Create request url string
    url = "https://www.bullionvault.com/secure/api/v2/cancel_order_xml.do"
    url *= "?orderId=" * orderId
    url *= "&confirmed=" * string(confirmed)

    # api access
    # Mockable for unit-testing
    response = @mock api_request("POST", url)

    # Dataframe for results
    df_cancel_order = DataFrame()

    # Parse return on success
    if (response[:status] == 200)
        # Check https://github.com/JuliaIO/LightXML.jl for reference

        # Step through tree to order
        try
            xdoc = parse_string(response[:body])
            envelope = root(xdoc)
            message = find_element(envelope, "message")

            # Add order to the data frame
            order = find_element(message, "order")
            content = attributes_dict(order)
            df_cancel_order = DataFrame(content)
        catch e
            println("ERROR: Something went wrong while parsing the server feedback. "
                * "Are you logged in?")
            return DataFrame()
        end
    end

    return df_cancel_order
end


"""
    place_order(
        actionIndicator::AbstractString,
        considerationCurrency::AbstractString,
        securityId::AbstractString,
        quantity::AbstractString,
        limit::AbstractString,
        typeCode::AbstractString,
        clientTransRef::AbstractString,
        confirmed::Bool,
        goodUntil::AbstractString="",
    ) -> DataFrame

Place Order

# Arguments
- `actionIndicator::AbstractString`: One of `B` or `S` for buy (gold with currency) or sell
    (gold for currency).
- `considerationCurrency::AbstractString`: The currency to trade. Must be one of `USD`,
    `GBP`, `EUR`.
- `securityId::AbstractString`: The ID of the security to trade. May be any valid
    `security ID`.
- `quantity::AbstractString`: The quantity to trade, in kilos. 1.234 represents 1 kilo, 234
    grams. Must have no more than 3 decimal places.
- `limit::AbstractString`: The limit price for the bid or offer, as an integer.
- `typeCode::AbstractString`: One of `TIL_CANCEL` (Good until cancelled), `TIL_TIME` (Good
    until time), `IMMEDIATE` (Execute immediate) or `FILL_KILL` (Fill or kill).
- `clientTransRef::AbstractString`: Your reference code. Must be unique for this account.
- `confirmed::Bool`: For bots, this value must always be `true`.
- `goodUntil::AbstractString=""`: Must be blank unless the typeCode is `TIL_TIME`, in which
    case it must be a timestamp in the format `yyyy-MM-dd%20HH:mm%20UTC`. The time should
    be in UTC.

# Returns
- `DataFrame`: containing order details if successful, otherwise it is empty. Content as
    shown in the structure below:
```JuliaIO
DataFrame(
    actionIndicator=["B"],
    clientTransRef=["asdf"],
    considerationCurrency=["USD"],
    goodUntil=[""],
    lastModified=["2005-06-02 14:14:25 UTC"],
    limit=["13500"],
    orderId=["1080"],
    orderTime=["2005-06-02 14:14:24 UTC"],
    quantity=["0.001"],
    quantityMatched=["0.001"],
    securityId=["AUXLN"],
    statusCode=["DONE"],
    totalCommission=["0.11"],
    totalConsideration=["12.59"],
    tradeType=["ORDER_BOARD_TRADE"],
    typeCode=["TIL_CANCEL"],
)
```
"""
function place_order(
    actionIndicator::AbstractString,
    considerationCurrency::AbstractString,
    securityId::AbstractString,
    quantity::AbstractString,
    limit::AbstractString,
    typeCode::AbstractString,
    clientTransRef::AbstractString,
    confirmed::Bool,
    goodUntil::AbstractString="",
)
    # Create request url string
    url = "https://www.bullionvault.com/secure/api/v2/place_order_xml.do"
    url *= "?actionIndicator=" * actionIndicator
    url *= "&considerationCurrency=" * considerationCurrency
    url *= "&securityId=" * securityId
    url *= "&quantity=" * quantity
    url *= "&limit=" * limit
    url *= "&typeCode=" * typeCode
    url *= "&clientTransRef=" * clientTransRef
    url *= "&confirmed=" * string(confirmed)
    if (typeCode == "TIL_TIME")
        url *= "&goodUntil=" * goodUntil
    end

    # api access
    # Mockable for unit-testing
    response = @mock api_request("POST", url)

    # Dataframe for results
    df_place_order = DataFrame()

    # Parse return on success
    if (response[:status] == 200)
        # Check https://github.com/JuliaIO/LightXML.jl for reference

        # Step through tree to order
        try
            xdoc = parse_string(response[:body])
            envelope = root(xdoc)
            message = find_element(envelope, "message")

            # Add order to the data frame
            order = find_element(message, "order")
            content = attributes_dict(order)
            df_place_order = DataFrame(content)
        catch e
            println("ERROR: Something went wrong while parsing the server feedback. "
                * "Are you logged in?")
            return DataFrame()
        end
    end

    return df_place_order
end


"""
    view_balance(simple::Bool=true) -> [DataFrame, DataFrame, DataFrame]

view balance of account

# Arguments
- `simple::Bool=true`: Specify whether to return a simple balance response, including client
    positions but without any pending settlement information. For the vast majority of bot
    users this extra information is not required and places extra load on our servers, It is
    __strongly__ advised to pass `simple=true`.

# Returns
- `[DataFrame, DataFrame, DataFrame]`: the three `DataFrame` contain
    `clientPositions`, `pendingSettlements` and `pendingTransfers`. This example shows their
    structure:

```JuliaIO
df_client_position = DataFrame(
    available=["3.026", "5"],
    classNarrative=["GOLD", "GOLD"],
    securityId=["AUXLN", "AUXNY"],
    total=["3.026", "5"],
    totalValuation=["40578.66", "67050"],
    valuationCurrency=["USD", "USD"]
)

df_pending_settlements = DataFrame(
    classNarrative=["GOLD", "GOLD"],
    securityId=["AUXZU", "AUXLN"],
    total=["250.919", "0.92"],
    totalValuation=["3181652.92", "11656.4"],
    valuationCurrency=["USD", "USD"],
    pendingTransferId=[1, 2]
)

df_pending_transfers = DataFrame(
    balance=["200", "50", "0.92"],
    dueDate=[
        "2012-10-18 00:00:00 UTC",
        "2012-10-18 00:00:00 UTC",
        "2012-11-08 00:00:00 UTC",
    ],
    lowestLedger=["UNS_RCT", "UNS_RCT", "UNS_RCT"],
    type=["OFF_MARKET_TRADE", "OFF_MARKET_TRADE", "OFF_MARKET_TRADE"],
    valuation=["2536000", "634000", "11656.4"],
    valuationCurrency=["USD", "USD", "USD"],
    pendingTransferId=[1, 1, 2]
)
```
"""
function view_balance(simple::Bool=true)
    # Create request url string
    url = "https://www.bullionvault.com/secure/api/v2/view_balance_xml.do"
    if (!simple)
        url *= "?simple=false"
    end

    # api access
    # Mockable for unit-testing
    response = @mock api_request("POST", url)

    # Dataframes for results
    df_client_positions = DataFrame()
    df_pending_settlements = DataFrame()
    df_pending_transfers = DataFrame()

    # Parse return on success
    if (response[:status] == 200)
        # Check https://github.com/JuliaIO/LightXML.jl for reference

        # Step through tree to client balance
        xdoc = parse_string(response[:body])
        envelope = root(xdoc)
        message = find_element(envelope, "message")
        clientBalance = find_element(message, "clientBalance")

        # Add all client positions to the data frame
        clientPositions = find_element(clientBalance, "clientPositions")
        for clientPosition in child_elements(clientPositions)
            content = attributes_dict(clientPosition)
            df = DataFrame(content)
            append!(df_client_positions, df)
        end

        # Add all pending settlements including transfers and connect them via an unique id
        pendingSettlements = find_element(clientBalance, "pendingSettlements")
        pending_transfer_id = 1
        for pendingSettlement in child_elements(pendingSettlements)
            dict_id = Dict("pendingTransferId" => pending_transfer_id)

            dict_content = attributes_dict(pendingSettlement)
            content = merge(dict_id, dict_content)
            df = DataFrame(content)
            append!(df_pending_settlements, df)

            pendingTransfers = find_element(pendingSettlement, "pendingTransfers")
            for pendingTransfer in child_elements(pendingTransfers)
                dict_content = attributes_dict(pendingTransfer)
                content = merge(dict_id, dict_content)
                df = DataFrame(content)
                append!(df_pending_transfers, df)
            end

            pending_transfer_id += 1
        end
    end

    return [df_client_positions, df_pending_settlements, df_pending_transfers]
end


"""
    view_orders(
        securityId::AbstractString="",
        considerationCurrency::AbstractString="",
        status::AbstractString="OPEN",
        fromDate::AbstractString="",
        toDate::AbstractString="",
        page::Integer=0,
    ) -> DataFrame

View orders

# Arguments
- `securityId::AbstractString=""`: The ID of the security to view. May be any valid
    `security ID`, or blank for all.
- `considerationCurrency::AbstractString=""`: The currency to view. May be one of `USD`,
    `GBP`, `EUR` or blank for all.
- `status::AbstractString="OPEN"`: Filters the list of orders returned. One of `OPEN` (show
    all open orders), `DEALT` (show all orders that have dealt), `OPEN_DEALT` (open + dealt
    orders), `CLOSED` (orders that are now closed), `REJECTED` (orders that were rejected)
    or blank for all. It is __strongly__ recommended that bot writers use only the `OPEN`
    status, as it is specially optimized for bot use. Other orders should be viewed with the
    `view_single_order` message.
- `fromDate::AbstractString=""`: Optional parameter to filter the non open orders, only
    returning those placed after the given date. __If not provided, will be set to 30 days
    ago.__ Date parameters do not apply to open orders.
- `toDate::AbstractString=""`: Optional parameter to filter the non open orders, only
    returning those placed before the given date. __Please note:__ The maximum difference
    between `fromDate` and `toDate` is 31 days. Date parameters do not apply to open orders.
- `page::Integer=0`: The response to view orders is paginated, starting at page zero. Use
    this parameter to select the page.

# Returns
- `DataFrame`: containing order details as shown in the structure below:
```JuliaIO
DataFrame(
    actionIndicator=["B", "B", "B", "S",  "S", "S"],
    considerationCurrency="USD",
    limit=["12510", "12500", "12490", "12590", "12600", "12610"],
    quantity=["0.1", "0.2", "0.1", "0.2", "0.1", "0.1"],
    securityId="AUXLN"
)
```
"""
function view_orders(
    securityId::AbstractString="",
    considerationCurrency::AbstractString="",
    status::AbstractString="OPEN",
    fromDate::AbstractString="",
    toDate::AbstractString="",
    page::Integer=0,
)
    # Create request url string
    url = "https://www.bullionvault.com/secure/api/v2/view_orders_xml.do"
    first = true
    if (!isempty(securityId))
        first = false
        url *= "?securityId=" * securityId
    end
    if (!isempty(considerationCurrency))
        url *= first ? "?" : "&"
        first = false
        url *= "considerationCurrency=" * considerationCurrency
    end
    if (!isempty(status))
        url *= first ? "?" : "&"
        first = false
        url *= "status=" * status
    end
    if (!isempty(fromDate))
        url *= first ? "?" : "&"
        first = false
        url *= "fromDate=" * fromDate
    end
    if (!isempty(toDate))
        url *= first ? "?" : "&"
        first = false
        url *= "toDate=" * toDate
    end
    url *= first ? "?" : "&"
    url *= "page=" * string(page)

    # api access
    # Mockable for unit-testing
    response = @mock api_request("POST", url)

    # Dataframes for results
    df_orders = DataFrame()

    # Parse return on success
    if (response[:status] == 200)
        # Check https://github.com/JuliaIO/LightXML.jl for reference

        # Step through tree to client balance
        xdoc = parse_string(response[:body])
        envelope = root(xdoc)
        message = find_element(envelope, "message")

        # Add all orders to the data frame
        orders = find_element(message, "orders")
        dict_clientId = attributes_dict(orders)
        for order in child_elements(orders)
            dict_order = attributes_dict(order)
            content = merge(dict_clientId, dict_order)
            df = DataFrame(content)
            append!(df_orders, df)
        end
    end

    return df_orders
end


"""
    view_single_order(orderId::AbstractString, clientTransRef::AbstractString) -> DataFrame

View single order

# Arguments
- `orderId::AbstractString`: The order ID returned by the `place_order` response.
- `clientTransRef::AbstractString`: The transaction reference used in the `place_order`
    request (optional).

# Returns
- `DataFrame`: containing order details as shown in the structure below:
```JuliaIO
DataFrame(
    actionIndicator=["B"],
    clientTransRef=["asdf"],
    considerationCurrency=["USD"],
    goodUntil=[""],
    lastModified=["2005-06-02 14:14:25 UTC"],
    limit=["13500"],
    orderId=["1080"],
    orderTime=["2005-06-02 14:14:24 UTC"],
    quantity=["0.001"],
    quantityMatched=["0.001"],
    securityId=["AUXLN"],
    statusCode=["DONE"],
    totalCommission=["0.11"],
    totalConsideration=["12.59"],
    tradeType=["ORDER_BOARD_TRADE"],
    typeCode=["TIL_CANCEL"]
)
```
"""
function view_single_order(orderId::AbstractString, clientTransRef::AbstractString = "")
    # Create request url string
    url = "https://www.bullionvault.com/secure/api/v2/view_single_order_xml.do"
    url *= "?orderId=" * orderId
    if (!isempty(clientTransRef))
        url *= "&clientTransRef=" * clientTransRef
    end

    # api access
    # Mockable for unit-testing
    response = @mock api_request("POST", url)

    # Dataframe for results
    df_view_single_order = DataFrame()

    # Parse return on success
    if (response[:status] == 200)
        # Check https://github.com/JuliaIO/LightXML.jl for reference

        # Step through tree to order
        try
            xdoc = parse_string(response[:body])
            envelope = root(xdoc)
            message = find_element(envelope, "message")

            # Add order to the data frame
            order = find_element(message,  "order")
            content = attributes_dict(order)
            df_view_single_order = DataFrame(content)
        catch e
            println("ERROR: Something went wrong while parsing the server feedback. "
                * "Are you logged in?")
            return DataFrame()
        end
    end

    return df_view_single_order
end


"""
    view_weight_unit() -> String

View weight unit setting

# Returns
- `String`: The unit of weight preference to set for the account. One of `KG` and `TOZ`
"""
function view_weight_unit()
    # Create request url string
    url = "https://www.bullionvault.com/secure/api/v2/view_weight_unit_xml.do"

    # api access
    # Mockable for unit-testing
    response = @mock api_request("POST", url)

    # Dataframe for results
    weight_unit = ""

    # Parse return on success
    if (response[:status] == 200)
        # Check https://github.com/JuliaIO/LightXML.jl for reference

        # Step through tree to value
        xdoc = parse_string(response[:body])
        envelope = root(xdoc)
        message = find_element(envelope, "message")

        # Add order to the data frame
        unitOfWeightSetting = find_element(message, "unitOfWeightSetting")
        weight_unit = attribute(unitOfWeightSetting, "value")
    end

    return weight_unit
end


"""
    update_weight_unit(newUnitOfWeight::AbstractString) -> String

Update weight unit setting

# Arguments
- `newUnitOfWeight`: The unit of weight preference to set for the account. One of `KG` and
    `TOZ`

# Returns
- `String`: The unit of weight preference to set for the account. One of `KG` and `TOZ`
"""
function update_weight_unit(newUnitOfWeight::AbstractString)
    # Create request url string
    url = "https://www.bullionvault.com/secure/api/v2/update_weight_unit_xml.do"
    url *= "?newUnitOfWeight=" * newUnitOfWeight

    # api access
    # Mockable for unit-testing
    response = @mock api_request("POST", url)

    # Dataframe for results
    weight_unit = ""

    # Parse return on success
    if (response[:status] == 200)
        # Check https://github.com/JuliaIO/LightXML.jl for reference

        # Step through tree to value
        xdoc = parse_string(response[:body])
        envelope = root(xdoc)
        message = find_element(envelope, "message")

        # Add order to the data frame
        unitOfWeightSetting = find_element(message, "unitOfWeightSetting")
        weight_unit = attribute(unitOfWeightSetting, "value")
    end

    return weight_unit
end


"""
    downtime() -> Array{Dict{String, String}}

This RSS feed announces planned downtime.

# Returns
- `Array{Dict{String, String}}`: with details about status:
```JuliaIO
Dict(
    "title"       => "BullionVault Planned Downtime",
    "link"        => "https://www.bullionvault.com",
    "description" => "Upcoming BullionVault Planned Downtime",
)
```
"""
function downtime()
    # Create request url string
    url = "https://www.bullionvault.com/planned-downtime-feed.do"

    # api access
    # Mockable for unit-testing
    response = @mock api_request("GET", url)

    # Dataframe for results
    info = Array{Dict{String, String}}

    # Parse return on success
    if (response[:status] == 200)
        # Check https://github.com/JuliaIO/LightXML.jl for reference

        # Step through tree to value
        body = String(response[:body])
        xdoc = parse_string(body)
        rss = root(xdoc)
        channel = find_element(rss, "channel")
        for data in child_elements(channel)
            dict = Dict(name(data) => content(data))
            info = merge(info, dict)
        end
    end

    return info
end
