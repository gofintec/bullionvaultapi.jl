```@meta
CurrentModule = BullionVaultAPI
```

# API

## Fields

| Security ID | Metal	 | Location  |
|-------------|----------|-----------|
| AUXZU	      | Gold	 | Zurich    |
| AUXLN	      | Gold	 | London    |
| AUXNY	      | Gold	 | New York  |
| AUXTR	      | Gold	 | Toronto   |
| AUXSG	      | Gold	 | Singapore |
| AGXZU	      | Silver	 | Zurich    |
| AGXLN	      | Silver	 | London    |
| AGXTR	      | Silver	 | Toronto   |
| AGXSG	      | Silver	 | Singapore |
| PTXLN       | Platinum | London    |


| statusCode         | Description                                         |
| ------------------ | --------------------------------------------------- |
| OPEN               | Order is open.                                      |
| DONE               | Order has closed.                                   |
| EXPIRED            | Order closed by expiring.                           |
| CANCELLED          | Order was cancelled.                                |
| KILLED             | Order was killed because it could not be filled.    |
| NOFUNDS            | Order was rejected due to insufficient funds.       |
| BADLIMIT           | Order was rejected due to limit too high/low.       |
| SILVER\_RESTRICTED | This account may not trade silver.                  |
| QUEUED             | Order is queued awaiting processing.                |
| AGIP\_ENABLED      | Selling is not allowed when Auto-Invest is enabled. |


| Value               | Description.                                             |
| ------------------- | -------------------------------------------------------- |
| ORDER\_BOARD\_TRADE | Order placed on the market order board (standard order). |
| ORDER\_AT\_FIX      | Order at Fix price.                                      |
| CLIENT\_ORDER       | Spot Market order.                                       |


### Rate limiting

To protect the performance of the website, actions made either by the web GUI, or via the API, are limited to a maximum number per minute. If this rate is exceeded then the action will be ignored and an error message shown, returning HTTP status code 429. The limit is reset after a minute.

The table below shows the maximum rate allowed for each action.

| Action            | Maximum rate per minute                 |
| ----------------- | --------------------------------------- |
| View market       | 60                                      |
| View balance      | 10                                      |
| Place order       | 10 per security ID currency combination |
| View orders       | 30                                      |
| View single order | 60 per security ID currency combination |

Place order and view single order are rate limited per security ID currency combination. For example, 10 calls to place order may be placed in one minute for AUXZU USD as well as 10 calls to AUXZU GBP, as well as 10 calls to AUXLN USD.

When we reasonably assume multiple client accounts are operated by the same individual, rates will be aggregated across those accounts.

We reserve the right to change these terms without notice and suspend client accounts who are using excessive system resources (e.g. are regularly being rate limited).


## Methods Overview
```@index
```

## Methods

```@autodocs
Modules = [BullionVaultAPI]
```
