using BullionVaultAPI
using Documenter

DocMeta.setdocmeta!(BullionVaultAPI, :DocTestSetup, :(using BullionVaultAPI); recursive=true)

makedocs(;
    modules=[BullionVaultAPI],
    authors="Oliver Griebel",
    repo="https://gitlab.com/gofintec/bullionvaultapi.jl/blob/{commit}{path}#{line}",
    sitename="BullionVaultAPI.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://gofintec.gitlab.io/bullionvaultapi.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
        "API" => "api.md",
    ],
)
